terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.102.0"
    }
  }
}

provider "azurerm" {
  features {
    
  }
}



resource "azurerm_resource_group" "rg" {
  name     = "app-gw-vm-db"
  location = "eastus2"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "myVNet"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "frontend" {
  name                 = "myAGSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_subnet" "backend" {
  name                 = "myBackendSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_storage_account" "appstore" {
  name                     = "appstore21872187"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  //public_network_access_enabled = true


  identity {
    type = "SystemAssigned"
  }
  depends_on = [azurerm_virtual_network.vnet, azurerm_subnet.backend]
}

resource "azurerm_storage_container" "data" {
  name                  = "data"
  storage_account_name  = "appstore21872187"
  container_access_type = "blob"
  depends_on = [
    azurerm_storage_account.appstore
  ]
}

# Here we are uploading our IIS Configuration script as a blob
# to the Azure storage account

resource "azurerm_storage_blob" "IIS_config1" {
  name                   = "IIS_Config1.ps1"
  storage_account_name   = "appstore21872187"
  storage_container_name = "data"
  type                   = "Block"
  source                 = "./IIS_Config1.ps1"
  depends_on             = [azurerm_storage_container.data]
}

resource "azurerm_storage_blob" "IIS_config2" {
  name                   = "IIS_Config2.ps1"
  storage_account_name   = "appstore21872187"
  storage_container_name = "data"
  type                   = "Block"
  source                 = "./IIS_Config2.ps1"
  depends_on             = [azurerm_storage_container.data]
}

resource "azurerm_public_ip" "pip" {
  name                = "myAGPublicIPAddress"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_network_interface" "nic" {
  count               = 2
  name                = "nic-${count.index + 1}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "nic-ipconfig-${count.index + 1}"
    subnet_id                     = azurerm_subnet.backend.id
    private_ip_address_allocation = "Dynamic"
  }
}
resource "azurerm_network_security_group" "appgateway_vm_nsg" {
  name                = "appgateway-vm-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "allow_http"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  security_rule {
    name                       = "allow_tcp_65200_65535"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "65200-65535"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  security_rule {
    name                       = "rdp-Inbound"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  security_rule {
    name                       = "allow-vm-storage"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = azurerm_subnet.frontend.address_prefixes[0]
    destination_address_prefix = azurerm_subnet.backend.address_prefixes[0]
  }

  security_rule {
    name                       = "Outbound_Allow_Subnet_Any"
    priority                   = 104
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
}

resource "azurerm_subnet_network_security_group_association" "appgateway_association" {
  subnet_id                 = azurerm_subnet.frontend.id
  network_security_group_id = azurerm_network_security_group.appgateway_vm_nsg.id
}


# Application Gateway Setup
resource "azurerm_application_gateway" "example" {
  name                = "example-appgw"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku {
    name     = "Standard_v2"
    tier     = "Standard_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "appgw-ip-config"
    subnet_id = azurerm_subnet.frontend.id
  }

  frontend_port {
    name = var.frontend_port_name
    port = 80
  }

  frontend_ip_configuration {
    name                 = var.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.pip.id
  }

  backend_http_settings {
    name                  = var.http_setting_name1
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 60
  }

  http_listener {
    name                           = var.listener_name
    frontend_ip_configuration_name = var.frontend_ip_configuration_name
    frontend_port_name             = var.frontend_port_name
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = var.request_routing_rule_name
    rule_type                  = "Basic"
    http_listener_name         = var.listener_name
    backend_address_pool_name  = var.backend_address_pool_name1
    backend_http_settings_name = var.http_setting_name1
    priority                   = 1
  }

  backend_address_pool {
    name = var.backend_address_pool_name1
  }

}

#app-gw backend pools association to nic of vm
resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "nic-assoc" {
  count                   = 2
  network_interface_id    = azurerm_network_interface.nic[count.index].id
  ip_configuration_name   = "nic-ipconfig-${count.index + 1}"
  backend_address_pool_id = one(azurerm_application_gateway.example.backend_address_pool).id
  depends_on              = [azurerm_network_interface.nic]
}


resource "azurerm_windows_virtual_machine" "vm" {
  count               = 2
  name                = "myVM${count.index + 1}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = "Standard_DS1_v2"
  admin_username      = "azureadmin"
  admin_password      = "Password@1234"

  network_interface_ids = [
    azurerm_network_interface.nic[count.index].id,
  ]
  identity {
    type = "SystemAssigned"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }


  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
  depends_on = [azurerm_storage_account.appstore, azurerm_network_interface.nic, ]
}



resource "azurerm_virtual_machine_extension" "vm_extension1" {
  name                 = "appvm-extension1"
  virtual_machine_id   = azurerm_windows_virtual_machine.vm[0].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"
  depends_on = [azurerm_windows_virtual_machine.vm ,azurerm_storage_blob.IIS_config1
  ]
  settings = <<SETTINGS
    {
        "fileUris": ["https://${azurerm_storage_account.appstore.name}.blob.core.windows.net/data/IIS_Config1.ps1"],
          "commandToExecute": "powershell -ExecutionPolicy Unrestricted -file IIS_Config1.ps1"     
    }
SETTINGS

}

resource "azurerm_virtual_machine_extension" "vm_extension2" {
  name                 = "appvm-extension2"
  virtual_machine_id   = azurerm_windows_virtual_machine.vm[1].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"
  depends_on = [azurerm_windows_virtual_machine.vm , azurerm_storage_blob.IIS_config2
  ]
  settings = <<SETTINGS
    {
        "fileUris": ["https://${azurerm_storage_account.appstore.name}.blob.core.windows.net/data/IIS_Config2.ps1"],
          "commandToExecute": "powershell -ExecutionPolicy Unrestricted -file IIS_Config2.ps1"     
    }
SETTINGS

}


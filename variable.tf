variable "backend_address_pool_name1" {
    default = "myBackendPool1"
}
variable "backend_address_pool_name2" {
    default = "myBackendPool2"
}


variable "frontend_port_name" {
    default = "myFrontendPort"
}

variable "frontend_ip_configuration_name" {
    default = "myAGIPConfig"
}

variable "http_setting_name1" {
    default = "myHTTPsetting1"
}

variable "http_setting_name2" {
    default = "myHTTPsetting2"
}


variable "listener_name" {
    default = "myListener"
}

variable "request_routing_rule_name" {
    default = "myRoutingRule"
}
